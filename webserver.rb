package 'httpd'

service 'httpd' do
  action [:start, :enable]
end

file '/var/www/html/index.html' do
  content '<html>
  <body>
    <h1>Hello Chef Provisioned!</h1>
  </body>
</html>'
end

service 'firewalld' do
  action :stop
end
