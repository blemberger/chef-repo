#
# Cookbook Name:: zaphod
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

# Install and start Apache httpd
package 'httpd'

service 'httpd' do
  action [:start, :enable]
end

# Write the root html file in the html home directory
template '/var/www/html/index.html' do
    source 'index.html.erb'
end

# Transfer the png file to the html home directory
cookbook_file '/var/www/html/nuclear-explosion.jpg' do
  source 'nuclear-explosion.jpg'
end

# Stop the firewall service so the page will be served
service 'firewalld' do
  action :stop
end

# Disable the firewall service so it doesn't start again on reboot
service 'firewalld' do
  action :disable
end
